<?php return [
    'ddb7e2f1f87f375e292f5c7a69e88378' => [
        'query' => [
            'queryParams' => [
                'SELECT name FROM people',
            ],
            'executeParams' => null,
            'fetchMethod' => 'execute',
            'fetchParams' => [],
            'boundValues' => [],
        ],
        'position' => 0,
        'results' => [
            true,
        ],
    ],
    '8c23b34b419147a3c5c8a97d20abc6f4' => [
        'query' => [
            'queryParams' => [
                'SELECT name FROM people',
            ],
            'executeParams' => null,
            'fetchMethod' => 'fetch',
            'fetchParams' => [],
            'boundValues' => [],
        ],
        'position' => 0,
        'results' => [
            [
                'name' => 'adam',
            ],
            [
                'name' => 'peter',
            ],
        ],
    ],
];