<?php return [
    '960b778390b6dcd0a62d2139be55a585' => [
        'query' => [
            'queryParams' => [
                'SELECT :name as name',
            ],
            'executeParams' => [
                'name' => 'adam',
            ],
            'fetchMethod' => 'execute',
            'fetchParams' => [
                [
                    'name' => 'adam',
                ],
            ],
            'boundValues' => [],
        ],
        'position' => 0,
        'results' => [
            true,
        ],
    ],
    '9dfcf4aac5a6c591b3705bee5abe2be8' => [
        'query' => [
            'queryParams' => [
                'SELECT :name as name',
            ],
            'executeParams' => [
                'name' => 'adam',
            ],
            'fetchMethod' => 'fetch',
            'fetchParams' => [],
            'boundValues' => [],
        ],
        'position' => 0,
        'results' => [
            [
                'name' => 'adam',
            ],
        ],
    ],
];