<?php


namespace PdoReplay;


use PDO;
use Symfony\Component\VarExporter\VarExporter;

class PdoRecorder extends PDO
{
    private $upstream;
    private $replayFile;

    public function __construct(PDO $upstream, string $replayFile)
    {
        $this->upstream = $upstream;
        $this->replayFile = $replayFile;

        if (file_exists($this->replayFile)) {
            unlink($this->replayFile);
        }
    }

    /**
     * @inheritDoc
     */
    public function prepare($statement, $options = [])
    {
        $result = $this->upstream->prepare($statement, $options);
        if (is_bool($result)) {
            return $result;
        }
        return new StatementRecorder($result, $this, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function beginTransaction()
    {
        return $this->upstream->beginTransaction();
    }

    /**
     * @inheritDoc
     */
    public function commit()
    {
        return $this->upstream->commit();
    }

    /**
     * @inheritDoc
     */
    public function rollBack()
    {
        return $this->upstream->rollBack();
    }

    /**
     * @inheritDoc
     */
    public function inTransaction()
    {
        return $this->upstream->inTransaction();
    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value)
    {
        return $this->upstream->setAttribute($attribute, $value);
    }

    /**
     * @inheritDoc
     */
    public function exec($statement)
    {
        $result = $this->upstream->exec($statement);
        $this->recordResult($result, 'exec', $statement);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function query($statement, $mode = PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null, array $ctorargs = array())
    {
        $pdoStatement = call_user_func_array([$this->upstream, 'query'], func_get_args());
        return new StatementRecorder($pdoStatement, $this, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function lastInsertId($name = null)
    {
        return $this->upstream->lastInsertId($name);
    }

    /**
     * @inheritDoc
     */
    public function errorCode()
    {
        return $this->upstream->errorCode();
    }

    /**
     * @inheritDoc
     */
    public function errorInfo()
    {
        return $this->upstream->errorInfo();
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($attribute)
    {
        return $this->upstream->getAttribute($attribute);
    }

    /**
     * @inheritDoc
     */
    public function quote($string, $parameter_type = PDO::PARAM_STR)
    {
        return $this->upstream->quote($string, $parameter_type);
    }

    /**
     * @inheritDoc
     */
    public function sqliteCreateFunction($function_name, $callback, $num_args = -1, $flags = 0)
    {
        return $this->upstream->sqliteCreateFunction($function_name, $callback, $num_args, $flags);
    }

    public function recordResult($result, $fetchMethod, $fetchParams, $queryParams = null, $executeParams = null, $boundValues = [])
    {
        if (!file_exists($this->replayFile)) {
            file_put_contents($this->replayFile, '<?php return [];');
        }
        $data = include $this->replayFile;

        // create key
        list($key, $md5Key) = self::getRecordingKey($queryParams, $executeParams, $fetchMethod, $fetchParams, $boundValues);

        // append record
        $data[$md5Key]['query'] = $key;
        $data[$md5Key]['position'] = 0;
        $data[$md5Key]['results'][] = $result;

        file_put_contents($this->replayFile, '<?php return ' . VarExporter::export($data) . ';');
    }

    public static function getRecordingKey($queryParams, $executeParams, $fetchMethod, $fetchParams, $boundValues)
    {
        $key = [
            'queryParams' => $queryParams,
            'executeParams' => $executeParams,
            'fetchMethod' => $fetchMethod,
            'fetchParams' => $fetchParams,
            'boundValues' => $boundValues,
        ];
        $md5Key = md5(json_encode($key));
        return [$key, $md5Key];
    }

}
