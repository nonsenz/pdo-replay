<?php


namespace PdoReplay;


use PDO;
use RuntimeException;
use Symfony\Component\VarExporter\VarExporter;

class PdoReplay extends PDO
{
    private $replayData;

    public function __construct(string $replayFile)
    {
        if (!file_exists($replayFile)) {
            file_put_contents($replayFile, '<?php return [];');
        }
        $this->replayData = include $replayFile;
    }

    /**
     * @inheritDoc
     */
    public function prepare($statement, $options = [])
    {
        return new StatementReplay($this, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function beginTransaction()
    {

    }

    /**
     * @inheritDoc
     */
    public function commit()
    {

    }

    /**
     * @inheritDoc
     */
    public function rollBack()
    {

    }

    /**
     * @inheritDoc
     */
    public function inTransaction()
    {

    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value)
    {

    }

    /**
     * @inheritDoc
     */
    public function exec($statement)
    {
        return $this->replayResult('exec', $statement);
    }

    /**
     * @inheritDoc
     */
    public function query($statement, $mode = PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null, array $ctorargs = array())
    {
        return new StatementReplay($this, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function lastInsertId($name = null)
    {

    }

    /**
     * @inheritDoc
     */
    public function errorCode()
    {

    }

    /**
     * @inheritDoc
     */
    public function errorInfo()
    {

    }

    /**
     * @inheritDoc
     */
    public function getAttribute($attribute)
    {

    }

    /**
     * @inheritDoc
     */
    public function quote($string, $parameter_type = PDO::PARAM_STR)
    {

    }

    /**
     * @inheritDoc
     */
    public function sqliteCreateFunction($function_name, $callback, $num_args = -1, $flags = 0)
    {

    }

    public function replayResult(string $fetchMethod, $fetchParams, $queryParams = null, $executeParams = null, $boundValues = [])
    {
        list($key, $md5Key) = PdoRecorder::getRecordingKey($queryParams, $executeParams, $fetchMethod, $fetchParams, $boundValues);

        if (!isset($this->replayData[$md5Key])) {
            throw new RuntimeException(''
                . "No replay for:\n"
                . VarExporter::export($key) . "\n\n"
                . "Having replays for:\n"
                . '- ' . implode(
                    "\n- ",
                    array_map(function($record) { return VarExporter::export($record['query']); }, $this->replayData)
                )
            );
        }

        $index = $this->replayData[$md5Key]['position'];
        if (!isset($this->replayData[$md5Key]['results'][$index])) {
            throw new RuntimeException(''
                . "No replay at position: $index for:\n"
                . VarExporter::export($key)
            );
        }

        $result = $this->replayData[$md5Key]['results'][$index];
        $this->replayData[$md5Key]['position']++;

        return $result;
    }


}