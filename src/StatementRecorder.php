<?php


namespace PdoReplay;


use PDO;
use PDOStatement;
use RuntimeException;

class StatementRecorder extends PDOStatement
{
    private $upstream;
    private $pdoRecorder;
    private $queryParams;

    private $executeParams;
    private $boundValues = [];

    public function __construct(PDOStatement $upstream, PdoRecorder $pdoRecorder, $queryParams = [])
    {
        $this->upstream = $upstream;
        $this->pdoRecorder = $pdoRecorder;
        $this->queryParams = $queryParams;

        $this->executeParams = null;
        $this->boundValues = [];
    }

    /**
     * @inheritDoc
     */
    public function execute($params = null)
    {
        $this->executeParams = $params;
        $result = $this->upstream->execute($params);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetch($fetch_style = null, $cursor_orientation = PDO::FETCH_ORI_NEXT, $cursor_offset = 0)
    {
        $result = $this->upstream->fetch($fetch_style, $cursor_orientation, $cursor_offset);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetchColumn($column_number = 0)
    {
        $result = $this->upstream->fetchColumn($column_number);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetchAll($how = null, $class_name = null, $ctor_args = null)
    {
        $result = $this->upstream->fetchAll($how = null, $class_name = null, $ctor_args = null);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetchObject($class_name = null, $ctor_args = null)
    {
        $result = $this->upstream->fetchObject($class_name, $ctor_args);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function rowCount()
    {
        $result = $this->upstream->rowCount();
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function columnCount()
    {
        $result = $this->upstream->columnCount();
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getColumnMeta($column)
    {
        $result = $this->upstream->getColumnMeta($column);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function bindParam($parameter, &$variable, $data_type = PDO::PARAM_STR, $length = null, $driver_options = null)
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindColumn($column, &$param, $type = null, $maxlen = null, $driverdata = null)
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindValue($parameter, $value, $data_type = PDO::PARAM_STR)
    {
        $this->boundValues[$parameter] = ['value' => $value, 'type' => $data_type];
        $this->upstream->bindValue($parameter, $value, $data_type);
    }

    /**
     * @inheritDoc
     */
    public function errorCode()
    {
        return $this->upstream->errorCode();
    }

    /**
     * @inheritDoc
     */
    public function errorInfo()
    {
        return $this->upstream->errorInfo();
    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value)
    {
        return $this->upstream->setAttribute($attribute, $value);
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($attribute)
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function setFetchMode($mode, $classNameObject = null, $ctorarfg = null)
    {
        return $this->upstream->setFetchMode($mode, $classNameObject, $ctorarfg);
    }

    /**
     * @inheritDoc
     */
    public function nextRowset()
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function closeCursor()
    {
        return $this->upstream->closeCursor();
    }

    private function recordResult($result, string $method, array $fetchParams = [])
    {
        $this->pdoRecorder->recordResult($result, $method, $fetchParams, $this->getQueryParams(), $this->getExecuteParams(), $this->boundValues);
    }

    private function getQueryParams()
    {
        return $this->queryParams;
    }

    private function getExecuteParams()
    {
        return $this->executeParams;
    }

}