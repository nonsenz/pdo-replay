<?php


namespace PdoReplay;


use PDO;
use PDOStatement;
use RuntimeException;

class StatementReplay extends PDOStatement
{
    private $pdoReplay;
    private $queryParams;

    private $executeParams;
    private $boundValues;

    public function __construct(PdoReplay $pdoReplay, array $queryParams)
    {
        $this->pdoReplay = $pdoReplay;
        $this->queryParams = $queryParams;

        $this->executeParams = null;
        $this->boundValues = [];
    }

    /**
     * @inheritDoc
     */
    public function execute($params = null)
    {
        $this->executeParams = $params;
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetch($fetch_style = null, $cursor_orientation = PDO::FETCH_ORI_NEXT, $cursor_offset = 0)
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetchColumn($column_number = 0)
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetchAll($how = null, $class_name = null, $ctor_args = null)
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetchObject($class_name = null, $ctor_args = null)
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function rowCount()
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function columnCount()
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function getColumnMeta($column)
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function bindParam($parameter, &$variable, $data_type = PDO::PARAM_STR, $length = null, $driver_options = null)
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindColumn($column, &$param, $type = null, $maxlen = null, $driverdata = null)
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindValue($parameter, $value, $data_type = PDO::PARAM_STR)
    {
        $this->boundValues[$parameter] = ['value' => $value, 'type' => $data_type];
    }

    /**
     * @inheritDoc
     */
    public function errorCode()
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function errorInfo()
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value)
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($attribute)
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function setFetchMode($mode, $classNameObject = null, $ctorarfg = null)
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function nextRowset()
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function closeCursor()
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function debugDumpParams()
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    private function replayResult(string $fetchMethod, array $fetchParams = [])
    {
        return $this->pdoReplay->replayResult($fetchMethod, $fetchParams, $this->getQueryParams(), $this->getExecuteParams(), $this->boundValues);
    }

    private function getQueryParams()
    {
        return $this->queryParams;
    }

    private function getExecuteParams()
    {
        return $this->executeParams;
    }

}